from django.http import HttpResponse, JsonResponse
from .models import Tree, Node, TEMPORARY_ID_PREFIX
from django.views.generic import DetailView
from django.template import loader
import json


def index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render({}, request))


class TreeView(DetailView):
    model = Tree

    def get(self, request, **kwargs):
        return JsonResponse(self.get_object().get_recursive_dict())

    def put(self, request, **kwargs):
        body = json.loads(request.body)
        new_nodes = {k: v for k, v in body.iteritems() if TEMPORARY_ID_PREFIX in k}
        for i, n in body.iteritems():
            # process only existing nodes
            if not TEMPORARY_ID_PREFIX in i:
                try:
                    node = Node.objects.get(id=n['id'])
                except Node.DoesNotExists:
                    return JsonResponse({'error': 'node doesn\'t exist'}, status=400)

                if 'is_deleted' in n and not n['is_deleted'] and node.is_deleted:
                    # do not change value or add children to deleted node
                    # even if cache doesn't know that it was deleted
                    continue

                if n['value']:
                    node.value = n['value']

                if len(n['children']) != node.node_set.count():
                    node.add_children(n['children'], new_nodes)
                node.save()


        deleted_nodes = [] # let's collect IDs of deleted nodes
        for i, n in body.iteritems():
            if not TEMPORARY_ID_PREFIX in i:
                try:
                    node = Node.objects.get(id=n['id'])
                except Node.DoesNotExists:
                    return JsonResponse({'error': 'node doesn\'t exist'})

                if n['is_deleted']:
                    deleted_nodes += node.set_deleted()


        return JsonResponse({'new_nodes': {k: v['id'] for k, v in new_nodes.iteritems()}, 'deleted_nodes': deleted_nodes})

    def patch(self, request, **kwargs):
        for n in Node.objects.all():
            if n.is_created:
                n.delete()
            else:
                n.value = 'node%d' % n.id
                n.is_deleted = False
                n.save()

        return JsonResponse(self.get_object().get_recursive_dict())


class NodeView(DetailView):
    model = Node

    def get(self, request, **kwargs):
        if self.get_object().is_deleted:
            return  JsonResponse({'error': 'node is deleted'}, status=400)
        return JsonResponse(self.get_object().get_dict())
