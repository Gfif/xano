from django.conf.urls import url

from .views import TreeView, NodeView, index

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^tree/(?P<pk>[0-9]+)$', TreeView.as_view(), name='tree-detail'),
    url(r'^node/(?P<pk>[0-9]+)$', NodeView.as_view(), name='node-detail'),
]