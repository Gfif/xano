var Node = React.createClass({
  changeValue: function(event) {
    this.props.onChangeValue(event.target.value);
  },

  render: function () {
    let deletedClass = this.props.isDeleted ? 'deleted' : '';
    let activeNodeClass = this.props.selected === this.props.id ? 'active': '';
    let hiddenInputClass = this.props.selected === this.props.id ? '': 'hidden';
    let hiddenValueClass = this.props.selected === this.props.id ? 'hidden': '';
    let children = Object.keys(this.props.children).map(key => this.props.children[key])
      .filter(child => child)
      .map(child => (
        <Node id={child.id}
              value={child.value}
              children={child.children}
              isDeleted={child.is_deleted}
              onClick={this.props.onClick}
              selected={this.props.selected}
              onChangeValue={this.props.onChangeValue}
        />)
      );
    return (
      <li key={this.props.id}>
        <a href='#'>
            <pre className={deletedClass + ' ' + activeNodeClass} onClick={this.props.onClick(this.props.id)}>

              <div className={hiddenValueClass}>{this.props.id + ' ' + this.props.value}</div>
              <input
                type='text'
                defaultValue={this.props.value}
                onChange={this.changeValue}
                className={hiddenInputClass + ' form-control'}
                autofocus={true}
              />
          </pre>
        </a>
        <ul>{children}</ul>
      </li>
    );
  }
});

var Tree = React.createClass({

  TEMPORARY_ID_PREFIX: 'tmp',

  getInitialState: function() {
    return {
      dbRoot: {},

      flatten: {},
      root: {},

      count: 0
    }
  },

  componentDidMount() {
    this.updateDbRoot();
  },

  updateDbRoot() {
    fetch(`/tree/1`).then(root => root.json()).then(root => this.setState({dbRoot: root}));
  },

  apply: function() {
    let flatten = JSON.parse(JSON.stringify(this.state.flatten));
    flatten = Object.keys(flatten)
      .filter(nodeID => flatten[nodeID].is_modified)
      .reduce((res, nodeID) => (res[nodeID] = flatten[nodeID], res), {});

    // mark all nodes in cache as non modified
    for (let nid in this.state.flatten) {
      this.state.flatten[nid].is_modified = false;
    }
    this.setState({flatten: this.state.flatten});

    // preapre data for sending
    for (let nid in flatten) {
      let node = flatten[nid];
      if (node.children) {
        node.children = Object.keys(node.children);
      }
    }
    fetch('/tree/1', { method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(flatten)}
      ).then(response => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw Error();
        }
      }).then(resp => {
        for (let nid in resp.new_nodes) {
          let newNodeID = resp.new_nodes[nid];
          let node = this.state.flatten[nid];
          if (this.state.selected === nid)
            this.state.selected = newNodeID;
          this.state.flatten[newNodeID] = node;
          node.id = newNodeID;
          if (node.parent) {
            this.state.flatten[node.parent].children[newNodeID] = this.state.flatten[node.parent].children[nid];
            delete this.state.flatten[node.parent].children[nid];
          } else {
            this.state.root[newNodeID] = this.state.root[nid];
            delete this.state.root[nid];
          }
        }
        for (let nid in resp.new_nodes) {
          delete this.state.flatten[nid];
        }

        for (let did of resp.deleted_nodes) {
          if (did in this.state.flatten) this.state.flatten[did].is_deleted = true;
        }
        this.setState({selected: this.state.selected, flatten: this.state.flatten, root: this.state.root});
        this.updateDbRoot();
    })
      .catch(console.error);
  },

  reset: function() {
    fetch('/tree/1', {method: 'PATCH'}).then(response => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw Error();
        }
      }).then(dbRoot => this.setState({dbRoot: dbRoot, root: {}, flatten: {}, selected: undefined}))
      .catch(console.error);
  },

  loadNode: function(nodeID) {
    let addNode = this.addNode;
    return function() {
      fetch(`/node/${nodeID}`).then(response => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw Error();
        }
      }).then(node => addNode(node))
      .catch(console.error);
    }
  },

  selectNode: function(nodeID) {
    return () => {
      if (!(this.state.flatten[nodeID].is_deleted))
        this.setState({selected: nodeID});
    }
  },

  addNode: function(node) {
    if (node.id in this.state.flatten) {
      return
    }

    let root = this.state.root;
    let flatten = this.state.flatten;

    // prepare node: transform list of children to dict of children
    let new_children = {};
    for (let c in node.children) {
      new_children[node.children[c]] = null;
    }
    node.children = new_children;

    // connect node with parent
    if (node.parent in flatten) //  && node.id in flatten[node.parent].children)
      flatten[node.parent].children[node.id] = node;
    else {
      root[node.id] = node;
    }

    // connect node with children
    for (let cid in node.children) {
      if (cid in root) {
        node.children[cid] = root[cid];
        delete root[cid];
      }
    }

    // add to flatten
    flatten[node.id] = node;

    if (node.parent && this.state.flatten[node.parent] && this.state.flatten[node.parent].is_deleted) {
      this.deleteNode(node.parent);
    }

    // set state
    this.setState({root: root, flatten: flatten});
  },

  deleteNode: function(nodeID) {
    // use internal func to avoid rerendaring after deleting each child
    let del = node => {
      if (!node) return;
      node.is_deleted = true;
      node.is_modified = true;
      for (let c in node.children) {
        del(node.children[c]);
      }
    };
    del(this.state.flatten[nodeID]);
    this.setState({flatten: this.state.flatten});
  },

  deleteSelectedNode: function() {
    this.deleteNode(this.state.selected);
    this.setState({selected: undefined});
  },

  addChild: function() {
    let node = this.state.flatten[this.state.selected];
    node.is_modified = true;
    let child = {
      id: this.TEMPORARY_ID_PREFIX+this.state.count,
      value: '',
      children: {},
      parent: node.id,
      is_deleted: false,
      is_created: true,
      is_modified: true
    };
    node.children[child.id] = child;
    this.state.flatten[child.id] = child;
    this.setState({flatten: this.state.flatten, count: this.state.count + 1, selected: child.id});
  },

  onChangeValue: function(value) {
    let flatten = this.state.flatten;
    flatten[this.state.selected].value = value;
    flatten[this.state.selected].is_modified = true;


    this.setState({flatten: flatten});
  },

  render: function() {
    // console.log(this.state);
    let hiddenButtonsClass = this.state.selected ? '' : 'hidden';
    let dbNodes = Object.keys(this.state.dbRoot).map(key => this.state.dbRoot[key])
      .map(node => (
        <Node
          id={node.id}
          children={node.children}
          value={node.value}
          isDeleted={node.is_deleted}
          onClick={this.loadNode}
          onChangeValue={this.onChangeValue}
        />));
    let cachedNodes = Object.keys(this.state.root).map(key => this.state.root[key])
      .map(node => (
        <Node
          id={node.id}
          children={node.children}
          value={node.value}
          isDeleted={node.is_deleted}
          selected={this.state.selected}
          onClick={this.selectNode}
          onChangeValue={this.onChangeValue}
        />));

    return (
      <div>
        <div className='row'>
          <div className='col-sm-6'>
            <ul>{dbNodes}</ul>
            <div className='btn-group row-centered'>
              <button className='btn btn-default' onClick={this.reset}>Reset</button>
              <button className='btn btn-default' onClick={this.apply}>Apply</button>
            </div>
          </div>
          <div className='col-sm-6'>
            <ul>{cachedNodes}</ul>
            <div className={hiddenButtonsClass + ' btn-group row-centered'}>
              <button className='btn btn-default' onClick={this.deleteSelectedNode}>Delete</button>
              <button className='btn btn-default' onClick={this.addChild}>Add Child</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

React.render(<Tree />, document.body);