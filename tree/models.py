from __future__ import unicode_literals
from django.db import models

TEMPORARY_ID_PREFIX = 'tmp'

class Node(models.Model):
    value = models.CharField('value of node', max_length=10)
    parent = models.ForeignKey('Node', verbose_name='link to parent node', null=True, default=None)
    is_deleted = models.BooleanField('is node deleted', default=False)
    is_created = models.BooleanField('is node created by user', default=False)

    def __str__(self):
        return self.value

    def get_recursive_dict(self):
        d = {}
        for n in self.node_set.all():
            d[n.id] = n.get_recursive_dict()
        return {
            'id': self.id,
            'value': self.value,
            'is_deleted': self.is_deleted,
            'children': d
        }

    def get_dict(self):
        return {
            'id': self.id,
            'value': self.value,
            'is_deleted': self.is_deleted,
            'parent': self.parent.id if self.parent else None,
            'children': [c.id for c in self.node_set.all()]
        }

    def set_deleted(self):
        if self.is_deleted:
            return []
        self.is_deleted = True
        res = [self.id]
        for c in self.node_set.all():
            res += c.set_deleted()

        self.save()
        return res

    def add_children(self, children, new_nodes):
        for c in children:
            if c in new_nodes:
                child = new_nodes[c]
                child_node = Node(value=child['value'], parent=self, \
                                  is_created=child['is_created'], is_deleted=child['is_deleted'])
                child_node.save()
                child['id'] = child_node.id
                if child['children']:
                   child_node.add_children(child['children'], new_nodes)


class Tree(models.Model):
    root = models.ForeignKey(Node, verbose_name='link to root node')

    def __str__(self):
        return self.root.value

    def get_recursive_dict(self):
        return {self.root.id: self.root.get_recursive_dict()}
