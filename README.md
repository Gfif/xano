# Xano
A simple tree-based database.

## How to run localy?
1. Clone this repo
```
$ git clone git@gitlab.com:Gfif/xano.git
```
2. Create virtualenv and activate it
```
$ virtualenv env
$ . ./env/bin/activate
```
3. Install requirements
```
(env) $ cd xano
(env) $ pip install -r requirements.txt
```
4. Create settings_local.py
```
(env) $ cd xano
(env) $ mv settings_local.py.example mv settings_local.py
```
5. Edit settings_local.py if necessary
6. Create database and load initial data
```
(env) python manage.py migrate
(env) python manage.py loaddata trees.json nodes.json
```
6. Run Django dev server
```
(env) $ python manage.py runserver
```
7. Go to `http://localhost:8000`

## Where can I try it?
Just go to [xano.psviridov.com](http://xano.psviridov.com)
